import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function courseReducer(state = initialState.courses, action) {
  switch (action.type) {
    case types.CREATE_COURSE_SUCCESS:
      //copies current state and will concat the
      //value of action.course to it.
      return [...state, { ...action.course }];
    case types.UPDATE_COURSE_SUCCESS:
      //maps state and finds the id. If found, will return
      //a new array which contains the updated value.
      return state.map((course) =>
        course.id === action.course.id ? action.course : course
      );
    case types.LOAD_COURSES_SUCCESS:
      return action.courses;
    case types.DELETE_COURSE_OPTIMISTIC:
      return state.filter((course) => course.id !== action.course.id);
    default:
      return state;
  }
}
